package de.ubt.ai4.petter.recpro.filter.collaborativefilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro.filter.collaborativefilter", "de.ubt.ai4.petter.recpro.filter.lib.recproapi"})
public class CollaborativeFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollaborativeFilterApplication.class, args);
	}

}
