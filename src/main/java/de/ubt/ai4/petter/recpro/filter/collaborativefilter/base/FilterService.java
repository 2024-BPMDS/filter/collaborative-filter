package de.ubt.ai4.petter.recpro.filter.collaborativefilter.base;

import de.ubt.ai4.petter.recpro.filter.collaborativefilter.ExampleData;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.Similarity;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.SimilarityId;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.SimilarityType;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.User;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.service.IDataService;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.service.SimilarityRepository;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.similarities.AdjustedCosineSimilarity;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.similarities.CosineSimilarity;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.slopeOne.SlopeOne;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResult;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResultItem;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.time.Duration;
import java.time.Instant;
import java.util.*;


@Service
@AllArgsConstructor
public class FilterService {

    private SlopeOne slopeOne;
    private CosineSimilarity cosineSimilarity;
    private AdjustedCosineSimilarity adjustedCosineSimilarity;
    private SimilarityRepository similarityRepository;
    private IDataService dataService;

    public void slopeOneFilter() {
        slopeOne.slopeOne(5);
    }

    public FilterInstance userBased(FilterInstance filterInstance, int k) {
        User target = dataService.getUserById(filterInstance.getAssigneeId());
        double[] tRatings = mapToDoubleArray(target.getRatings());
        target.setMeanRating(Arrays.stream(tRatings).sum() / tRatings.length);
        target.setCosineSimilarity(cosineSimilarity.cosineSimilarity(target.getRatings(), target.getRatings()));

        List<User> users = dataService.getUsers();
        this.createCosineSimilarity(target, users, filterInstance.getFilterId());

        Tasklist tasklist = dataService.getTasklist(filterInstance.getTasklistId());

        filterInstance.setResult(new FilterResult());
        tasklist.getTasks().forEach(task -> {
            Double likeliness = this.generateRating(task.getActivityId(), k, users);
            if (!likeliness.isNaN()) {
                filterInstance.getResult().getItems().add(new FilterResultItem(null, task.getId(), likeliness, -1, "", true));
            } else {
                filterInstance.getResult().getItems().add(new FilterResultItem(null, task.getId(), null, -1, "", true));
            }
        });
        return filterInstance;
    }

    public Tasklist userBasedCollaborativeFilteringWithCosineSimilarity(int k, String userId, String filterId, Long tasklistId) {
        User target = dataService.getUserById(userId);
        double[] tRatings = mapToDoubleArray(target.getRatings());
        target.setMeanRating(Arrays.stream(tRatings).sum() / tRatings.length);
        target.setCosineSimilarity(cosineSimilarity.cosineSimilarity(target.getRatings(), target.getRatings()));

        List<User> users = dataService.getUsers();
        this.createCosineSimilarity(target, users, filterId);
        users.forEach(System.out::println);

        Tasklist tasklist = dataService.getTasklist(tasklistId);

        tasklist.getTasks().forEach(task -> {
            Double likeliness = this.generateRating(task.getActivityId(), k, users);
            if (!likeliness.isNaN()) {
                task.setLikeliness(likeliness);
            } else {
                task.setLikeliness(null);
            }
        });
        return tasklist;
    }

    public void createCosineSimilarity(User target, List<User> users, String filterId) {
        users.forEach(user -> {

            SimilarityId similarityId = new SimilarityId(target.getUserId(), user.getUserId(), SimilarityType.COSINE, filterId);
            Optional<Similarity> s = similarityRepository.findById(similarityId);
            if (s.isPresent()) {
                Instant now = Instant.now();
                Duration duration = Duration.between(s.get().getDate(), now);
                long daysDifference = duration.toSeconds();

                if (daysDifference < 100) {
                    System.out.println("USE OLD");
                }
            }

            double[] uRatings = mapToDoubleArray(user.getRatings());

            user.setMeanRating(Arrays.stream(uRatings).sum() / uRatings.length);

            Map<CharSequence, Double> help = new HashMap<>(target.getRatings());
            Map<CharSequence, Double> userRatings = new HashMap<>(user.getRatings());


            help.entrySet().removeIf(entry -> entry.getValue() == null);
            userRatings.entrySet().removeIf(entry -> entry.getValue() == null);

            Set<CharSequence> commonKeys = new HashSet<>(help.keySet());
            commonKeys.retainAll(userRatings.keySet());

            help.keySet().retainAll(commonKeys);
            userRatings.keySet().retainAll(commonKeys);

            Map<CharSequence, Double> helpDouble = new HashMap<>();
            help.forEach((a, b) -> helpDouble.put(a, b - target.getMeanRating()));

            Map<CharSequence, Double> userRatingsDouble = new HashMap<>();
            userRatings.forEach((a, b) -> userRatingsDouble.put(a, (b - user.getMeanRating())));

            user.setCosineSimilarity(cosineSimilarity.cosineSimilarity(help, userRatings));

            Similarity similarity = new Similarity(
                    new SimilarityId(target.getUserId(), user.getUserId(), SimilarityType.COSINE, filterId),
                    user.getCosineSimilarity(),
                    Instant.now()
            );

            similarityRepository.saveAndFlush(similarity);
        });
    }

    public void cosine() {

    }

    public void pearson(int k, String itemId) {

        User target = ExampleData.getTargetUser();
        double[] tRatings = mapToDoubleArray(target.getRatings());

        target.setMeanRating(Arrays.stream(tRatings).sum() / tRatings.length);

        List<User> users = ExampleData.getUsers();

        PearsonsCorrelation correlation = new PearsonsCorrelation();

        users.forEach(user -> {

            double[] uRatings = mapToDoubleArray(user.getRatings());

            user.setMeanRating(Arrays.stream(uRatings).sum() / uRatings.length);

            Map<CharSequence, Double> help = new HashMap<>(target.getRatings());
            Map<CharSequence, Double> userRatings = new HashMap<>(user.getRatings());


            help.entrySet().removeIf(entry -> entry.getValue() == null);
            userRatings.entrySet().removeIf(entry -> entry.getValue() == null);

            Set<CharSequence> commonKeys = new HashSet<>(help.keySet());
            commonKeys.retainAll(userRatings.keySet());

            help.keySet().retainAll(commonKeys);
            userRatings.keySet().retainAll(commonKeys);

            Map<CharSequence, Double> helpDouble = new HashMap<>();
            help.forEach((a, b) -> helpDouble.put(a, Double.valueOf(b - target.getMeanRating())));

            Map<CharSequence, Double> userRatingsDouble = new HashMap<>();
            userRatings.forEach((a, b) -> userRatingsDouble.put(a, (b - user.getMeanRating())));

            user.setCosineSimilarity(cosineSimilarity.cosineSimilarity(help, userRatings));
            user.setPearsonCorrelation(correlation.correlation(mapToDoubleArray(help), mapToDoubleArray(userRatings)));
        });

        Collections.sort(users, Comparator.comparing(User::getCosineSimilarity).reversed());

        System.out.println("COSINE: ");
        users.forEach(System.out::println);

        Collections.sort(users, Comparator.comparing(User::getPearsonCorrelation).reversed());
        System.out.println("################");
        System.out.println("PEARSON: ");
        users.forEach(System.out::println);

        this.generateRating(itemId, k, users);
    }

    private double[] mapToDoubleArray(Map<CharSequence, Double> map) {
        List<Double> values = new ArrayList<>(map.values());
        return values.stream().filter(Objects::nonNull).mapToDouble(value -> value).toArray();
    }

    private double generateRating(String itemId, int k, List<User> users) {
        List<User> u = new ArrayList<>();

        users.forEach(user -> {
            if (u.size() < k) {
                if (user.getRatings().get(itemId) != null) {
                    u.add(user);
                }
            }
        });

        double obenCosine = u.stream().mapToDouble(user -> user.getCosineSimilarity() * (user.getRatings().get(itemId))).sum();
        double untenCosine = u.stream().mapToDouble(User::getCosineSimilarity).sum();

        double resCosine = obenCosine / untenCosine;

        System.out.println(resCosine);

        double oben = u.stream().mapToDouble(user -> user.getPearsonCorrelation() * user.getRatings().get(itemId)).sum();
        double unten = u.stream().mapToDouble(User::getPearsonCorrelation).sum();

        double res = oben / unten;

        System.out.println(res);


        double obenAdjC = u.stream().mapToDouble(user -> user.getPearsonCorrelation() * (user.getRatings().get(itemId) - user.getMeanRating())).sum();
        double untenAdjC = u.stream().mapToDouble(user -> user.getPearsonCorrelation()).sum();

        double resAdjC = obenAdjC / untenAdjC;
        System.out.println(2 + resAdjC);

        return resCosine;
    }
}
