package de.ubt.ai4.petter.recpro.filter.collaborativefilter.model;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Similarity {

    @EmbeddedId
    private SimilarityId id;
    private double value;
    private Instant date;

}
