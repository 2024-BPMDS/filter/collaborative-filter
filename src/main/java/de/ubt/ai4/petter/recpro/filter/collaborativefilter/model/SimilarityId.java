package de.ubt.ai4.petter.recpro.filter.collaborativefilter.model;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SimilarityId implements Serializable {
    private String userIdI;
    private String userIdJ;
    @Enumerated(EnumType.STRING)
    private SimilarityType similarityType;
    private String filterId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimilarityId)) return false;
        SimilarityId that = (SimilarityId) o;
        return (Objects.equals(userIdI, that.userIdI) && Objects.equals(userIdJ, that.userIdJ) && Objects.equals(similarityType, that.similarityType) && Objects.equals(filterId, that.filterId)) ||
                (Objects.equals(userIdI, that.userIdJ) && Objects.equals(userIdJ, that.userIdI) && Objects.equals(similarityType, that.similarityType) && Objects.equals(filterId, that.filterId));
    }

    @Override
    public int hashCode() {
        return Objects.hash(userIdI, userIdJ, similarityType, filterId) + Objects.hash(userIdJ, userIdI, similarityType, filterId);
    }
}
