package de.ubt.ai4.petter.recpro.filter.collaborativefilter.service;

import com.opencsv.CSVReader;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.Rating;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.User;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.TaskState;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Profile("test")
public class DataServiceExampleImpl implements IDataService {

    private final ResourceLoader resourceLoader;

    public List<Rating> readFromCsv() {
        Resource resource = resourceLoader.getResource("classpath:/data/rating.csv");

        List<Rating> ratings = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new FileReader(resource.getFile()))) {
            List<String[]> records = reader.readAll();
            for (String[] record: records)
                if (record.length == 3) {
                    Rating rating = new Rating(record[0], record[1], Double.parseDouble(record[2]));
                    ratings.add(rating);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ratings;
    }

    public List<Rating> getRatingById(String userId) {
        List<Rating> ratings = this.readFromCsv();

        ratings = ratings.stream().filter(rating -> rating.getUserId().equals(userId)).toList();

        return ratings;
    }

    @Override
    public User getUserById(String userId) {
        User user = new User();
        user.setUserId(userId);
        user.setRatings(this.getRatingsById(userId));
        return user;
    }

    public Map<CharSequence, Double> getRatingsById(String userId) {
        List<Rating> ratings = this.getRatingById(userId);
        Map<CharSequence, Double> result = new HashMap<>();

        ratings.forEach(rating -> result.put(rating.getItemId(), rating.getValue()));
        return result;
    }

    @Override
    public List<User> getUsers() {
        List<Rating> ratings = this.readFromCsv();
        Map<String, List<Rating>> help = ratings.stream().collect(Collectors.groupingBy(Rating::getUserId));
        Map<String, Map<CharSequence, Double>> result = new HashMap<>();

        List<User> users = new ArrayList<>();

        help.forEach((key, value) -> {
            User u = new User();
            u.setUserId(key);
            Map<CharSequence, Double> ratingEntries = new HashMap<>();
            value.forEach(rating -> ratingEntries.put(rating.getItemId(), rating.getValue()));
            u.setRatings(ratingEntries);
            result.put(key, ratingEntries);
            users.add(u);
        });
        return users;
    }

    @Override
    public Tasklist getTasklist(Long tasklistId) {
        Tasklist tasklist = new Tasklist();
        tasklist.setId(tasklistId);

        tasklist.getTasks().add(new Task("TASKID", -1, TaskState.ASSIGNED, "1", "PROCESS_1", 50, Instant.now(), null, null, "EXECUTION_ID", "PROCESS_INSTANCE_ID"));

        tasklist.getTasks().add(new Task("TASKID_6", -1, TaskState.ASSIGNED, "6", "PROCESS_1", 50, Instant.now(), null, null, "EXECUTION_ID", "PROCESS_INSTANCE_ID"));
        return tasklist;
    }
    public List<Rating> getRatingByNotId(String userId) {
        List<Rating> ratings = this.readFromCsv();

        ratings.stream().filter(rating -> !rating.getUserId().equals(userId));

        return ratings;
    }


}
