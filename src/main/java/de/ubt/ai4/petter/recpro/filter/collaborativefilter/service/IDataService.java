package de.ubt.ai4.petter.recpro.filter.collaborativefilter.service;

import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.User;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;

import java.util.List;

public interface IDataService {

    Tasklist getTasklist(Long tasklistId);
    List<User> getUsers();
    User getUserById(String userId);
}
