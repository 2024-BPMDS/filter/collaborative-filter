package de.ubt.ai4.petter.recpro.filter.collaborativefilter.service;

import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.Similarity;
import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.SimilarityId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SimilarityRepository extends JpaRepository<Similarity, SimilarityId> {
}
